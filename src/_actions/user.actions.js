import { userConstants } from '../_constants';
import { agentServices } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const userActions = {
  login,
  logout,
  register,
  getAll,
  delete: _delete,
  getCities,
  addCities,
  addHolidays,
};

function login(username, password) {
  return (dispatch) => {
    dispatch(request({ username }));
    agentServices.login(username, password).then((response) => {
      if (response.data && response.data.data && response.data.data.user) {
        localStorage.setItem('user', JSON.stringify(response.data.data.user));
        dispatch(success(response.data.data.user));
        history.push('/');
        dispatch(alertActions.success('Login successful'));
      } else {
        dispatch(failure(response.data.toString()));
        dispatch(alertActions.error(response.data.toString()));
      }
    });
  };

  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  agentServices.logout();
  return { type: userConstants.LOGOUT };
}

function register(user) {
  return (dispatch) => {
    dispatch(request(user));

    userService.register(user).then((response) => {
      if (response.data && response.data.data && response.data.data.user) {
        dispatch(success());
        history.push('/login');
        dispatch(alertActions.success('Login successful'));
      } else {
        dispatch(failure(response.data.toString()));
        dispatch(alertActions.error(response.data.toString()));
      }
    });
  };

  function request(user) {
    return { type: userConstants.REGISTER_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.REGISTER_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.REGISTER_FAILURE, error };
  }
}

function getAll() {
  return (dispatch) => {
    dispatch(request());

    userService.getAll().then(
      (users) => dispatch(success(users)),
      (error) => dispatch(failure(error.toString()))
    );
  };

  function request() {
    return { type: userConstants.GETALL_REQUEST };
  }
  function success(users) {
    return { type: userConstants.GETALL_SUCCESS, users };
  }
  function failure(error) {
    return { type: userConstants.GETALL_FAILURE, error };
  }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return (dispatch) => {
    dispatch(request(id));

    userService.delete(id).then(
      (user) => dispatch(success(id)),
      (error) => dispatch(failure(id, error.toString()))
    );
  };

  function request(id) {
    return { type: userConstants.DELETE_REQUEST, id };
  }
  function success(id) {
    return { type: userConstants.DELETE_SUCCESS, id };
  }
  function failure(id, error) {
    return { type: userConstants.DELETE_FAILURE, id, error };
  }
}

function getCities() {
  return (dispatch) => {
    dispatch(request({}));
    agentServices.getAllCities().then((response) => {
      if (response.data && response.data.length > 0) {
        dispatch(success(response.data));
      } else {
        dispatch(failure(response.data.toString()));
      }
    });
  };
  function request(cities) {
    return { type: userConstants.GET_ALL_CITIES_REQUEST, cities };
  }
  function success(cities) {
    return { type: userConstants.GET_ALL_CITIES_SUCCESS, cities };
  }
  function failure(error) {
    return { type: userConstants.GET_ALL_CITIES_FAILURE, error };
  }
}
function addCities(city_name) {
  return (dispatch) => {
    agentServices.addCities(city_name).then((response) => {
      if (response.status === 200) {
        dispatch(getCities());
      }
    });
  };
}
function addHolidays(
  title,
  subtitle,
  short_desc,
  long_desc,
  amount,
  days,
  night,
  city_id,
  category
) {
  return (dispatch) => {
    userService
      .addHolidays(
        title,
        subtitle,
        short_desc,
        long_desc,
        amount,
        days,
        night,
        city_id,
        category
      )
      .then((response) => {
        if (response.data && response.data.data && response.data.data.user) {
          dispatch(success());
          history.push('/holidays/1/itinerary');
        } else {
          dispatch(failure(response.data.toString()));
        }
      });
  };

  function request(user) {
    return { type: userConstants.ADD_HOLIDAYS_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.ADD_HOLIDAYS_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.ADD_HOLIDAYS_FAILURE, error };
  }
}
