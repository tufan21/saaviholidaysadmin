import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import App from './components/App/App';
import Login from './components/LoginPage/Login.jsx';
import { history } from './_helpers';
import { Provider } from 'react-redux';
import { store } from './_helpers';
export default (
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact path='/login' component={Login} />
        <Route path='/' component={App} />
      </Switch>
    </Router>
  </Provider>
);
