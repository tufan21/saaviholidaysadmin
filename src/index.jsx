// import React from 'react';
// import { render } from 'react-dom';
// import { Provider } from 'react-redux';
// import { Router, Route, Switch } from "react-router-dom";
// import {store } from './_helpers';

// import {Main} from './Main'
// // import routes from "./components/routes";
// import { history } from './_helpers';
// // setup fake backend
// import { configureFakeBackend } from './_helpers';
// configureFakeBackend();

// render(
//     <Provider store={store}>
//        <Router history={history}>
//             <Main />
//         </Router>
//     </Provider>,
//     document.getElementById('root')

// );

import { render } from "react-dom";
import routes from "./routes";
import registerServiceWorker from "./registerServiceWorker";
import "./styles.css";
// import "font-awesome/css/font-awesome.css";
// require("./favicon.ico");

render(routes, document.getElementById("root"));
registerServiceWorker();
