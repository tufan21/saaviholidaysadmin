import { authHeader } from '../_helpers';
import { create } from 'apisauce';
import { apiConstants } from './config.js';
export const agentServices = {
  login,
  logout,
  register,
  getAll,
  getById,
  update,
  delete: _delete,
  getAllCities,
  addCities,
};
// define the api
const api = create({
  baseURL: apiConstants.baseURL,
  headers: {
    Accept: 'application/json',
    'Cache-Control': 'no-cache',
  },
  timeout: 30000,
});
const setHeader = () => {
  const bearerToken = JSON.parse(localStorage.user).auth_token;
  if (bearerToken != undefined) {
    api.setHeader('Authorization', `Bearer ${bearerToken}`);
    return true;
  }
  return false;
};
function login(username, password) {
  return api.post('/users/authenticate', {
    email: username,
    password: password,
  });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('user');
}
function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
}

function getAllCities() {
  if (setHeader()) {
    return api.get('/cities');
  }
}
function addCities(cityName) {
  if (setHeader()) {
    return api.post('/cities', { name: cityName });
  }
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(
    handleResponse
  );
}

function register(user) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user),
  };

  return fetch(`${config.apiUrl}/users/register`, requestOptions).then(
    handleResponse
  );
}

function update(user) {
  const requestOptions = {
    method: 'PUT',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(user),
  };

  return fetch(`${config.apiUrl}/users/${user.id}`, requestOptions).then(
    handleResponse
  );
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  const requestOptions = {
    method: 'DELETE',
    headers: authHeader(),
  };

  return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(
    handleResponse
  );
}
function addHolidays(
  title,
  subtitle,
  short_desc,
  long_desc,
  amount,
  days,
  night,
  city_id,
  category
) {
  if (setHeader()) {
    return api.post(`cities/${city_id}/holidays`, {
      title: title,
      subtitle: subtitle,
      short_desc: short_desc,
      long_desc: long_desc,
      price: amount,
      days: days,
      nights: night,
      category: category,
    });
  }
  return Promise.resolve({});
}
function handleResponse(response) {
  if (response.data && response.data.data) {
    return response.data.data.user;
  } else {
    const error = (data && data.message) || response.statusText;
    return Promise.reject(error);
  }
  // return response.data.then((text) => {
  //   const data = text && JSON.parse(text);
  //   if (!response.ok) {
  //     if (response.status === 401) {
  //       // auto logout if 401 response returned from api
  //       logout();
  //       location.reload(true);
  //     }

  //     const error = (data && data.message) || response.statusText;
  //     return Promise.reject(error);
  //   }

  //   return data;
  // });
}
