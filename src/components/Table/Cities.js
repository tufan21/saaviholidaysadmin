import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Data from '../../data';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import ContentAdd from '@material-ui/icons/Add';
import Grid from '@material-ui/core/Grid';
import tableColumnStyle from './BasicTables/tableColumnStyle.js';

import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { userActions } from '../../_actions/';
import { fade } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';

const styles = {
  floatingActionButton: {
    margin: 0,
    top: 'auto',
    right: 20,
    bottom: 20,
    left: 'auto',
    position: 'fixed',
  },
  buttons: {
    marginTop: 30,
    float: 'right',
  },
  saveButton: {
    marginLeft: 15,
    height: 35,
  },
};
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    width: 'auto',
    padding: '10px 12px',
    marginTop: 5,
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
    },
  },
  cities_header: {
    fontSize: 16,
  },
}))(InputBase);
class Cities extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      city: '',
      submitted: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this.props.getCities();
  }
  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }
  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });
    const { city } = this.state;
    if (city) {
      this.props.addCities(city);
    }
  }

  render() {
    const { city } = this.state;
    const { cityList } = this.props;
    console.log('city list');
    console.log(cityList);
    return (
      <div>
        <Grid container spacing={3}>
          <Grid item xs={6} sm={6} md={6}>
            <BootstrapInput
              defaultValue=''
              id='bootstrap-input'
              placeholder='Name'
              name='city'
              value={city}
              onChange={this.handleChange}
            />
            <Button
              style={styles.saveButton}
              variant='contained'
              type='submit'
              color='primary'
              onClick={this.handleSubmit.bind(this)}
            >
              Save
            </Button>
          </Grid>
          <Grid item xs={12} sm={12} md={12}>
            <Card>
              <CardContent>
                <Typography
                  color='textSecondary'
                  variant='h4'
                  component='h4'
                  gutterBottom
                >
                  Cities
                </Typography>
                <Divider />
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell style={tableColumnStyle.columns.header_id}>
                        ID
                      </TableCell>
                      <TableCell style={tableColumnStyle.columns.header_name}>
                        Name
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {cityList &&
                      cityList.map((cities) => (
                        <TableRow key={cities.id} hover={true}>
                          <TableCell style={tableColumnStyle.columns.id}>
                            {cities.id}
                          </TableCell>
                          <TableCell style={tableColumnStyle.columns.name}>
                            {cities.name}
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <Link to='/form'>
          <Button
            mini={true}
            variant='fab'
            style={styles.floatingActionButton}
            color='secondary'
          >
            <ContentAdd />
          </Button>
        </Link>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { cityList } = state.cities;
  return { cityList };
}

const mapDispatchToProps = {
  getCities: userActions.getCities,
  addCities: userActions.addCities,
};

Cities.propTypes = {
  children: PropTypes.element,
  classes: PropTypes.object,
};
export default connect(mapStateToProps, mapDispatchToProps)(Cities);
