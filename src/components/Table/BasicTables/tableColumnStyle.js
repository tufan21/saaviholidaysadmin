export default {
  columns: {
    id: {
      width: '10%',
      fontSize: 14,
    },
    header_id: {
      width: '10%',
      fontSize: 16,
    },
    name: {
      width: '40%',
      fontSize: 14,
    },
    header_name: {
      width: '40%',
      fontSize: 16,
    },
    price: {
      width: '20%',
    },
    category: {
      width: '20%',
    },
    edit: {
      width: '10%',
    },
  },
};
