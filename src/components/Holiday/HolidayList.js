import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';
import Data from '../../data';
import { withStyles } from '@material-ui/core/styles';
import styles from '../Table/BasicTables/tableColumnStyle.js';
import { history } from '../../_helpers/';
class HolidayList extends React.Component {
  handleClick = (event, id) => {
    this.props.history.push('/holidays/itinerary');
  };
  render() {
    return (
      <Card>
        <CardContent>
          <Typography color='textSecondary' gutterBottom>
            Holidays
          </Typography>
          <Divider />
          <Table>
            <TableHead>
              <TableRow>
                <TableCell style={styles.columns.id}>ID</TableCell>
                <TableCell style={styles.columns.name}>Title</TableCell>
                <TableCell style={styles.columns.price}>Price</TableCell>
                <TableCell style={styles.columns.category}>Days</TableCell>
                <TableCell style={styles.columns.category}>Nights</TableCell>
                <TableCell style={styles.columns.category}>Category</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Data.tablePage.items.slice(0, 5).map((item) => (
                <TableRow
                  key={item.id}
                  hover={true}
                  onClick={(event) => this.handleClick(event, item.id)}
                >
                  <TableCell style={styles.columns.id}>{item.id}</TableCell>
                  <TableCell style={styles.columns.name}>{item.name}</TableCell>
                  <TableCell style={styles.columns.price}>
                    {item.price}
                  </TableCell>
                  <TableCell style={styles.columns.price}>
                    {item.price}
                  </TableCell>
                  <TableCell style={styles.columns.price}>
                    {item.price}
                  </TableCell>
                  <TableCell style={styles.columns.category}>
                    {item.category}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(null, { withTheme: true })(HolidayList);
