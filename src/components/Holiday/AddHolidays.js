import React from 'react';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import { grey } from '@material-ui/core/colors';
import InputLabel from '@material-ui/core/InputLabel';
import PageBase from '../PageBase';
import Grid from '@material-ui/core/Grid';
import { Input, InputAdornment } from '@material-ui/core';
import { userActions } from '../../_actions/';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import InputBase from '@material-ui/core/InputBase';
import { makeStyles, withStyles } from '@material-ui/core/styles';
const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(1),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
    },
  },
}))(InputBase);

const styles = {
  toggleDiv: {
    marginTop: 20,
    marginBottom: 5,
  },
  toggleLabel: {
    color: grey[400],
    fontWeight: 100,
  },
  buttons: {
    marginTop: 30,
    float: 'right',
  },
  saveButton: {
    marginLeft: 5,
    marginTop: 30,
    float: 'right',
    width: 100,
    fontSize: 18,
  },
};
class AddHolidays extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      city: '',
      title: '',
      subtitle: '',
      short_desc: '',
      long_desc: '',
      amount: 0,
      days: '',
      night: '',
      city_id: 0,
      category: '',
    };
  }
  componentDidMount() {
    this.props.getCities();
  }
  handleCity = (evt) => {
    this.setState({
      city_id: evt.target.value,
    });
  };
  handleCategory = (evt) => {
    this.setState({
      category: evt.target.value,
    });
  };
  handleDays = (evt) => {
    this.setState({
      days: evt.target.value,
    });
  };
  handleDays = (evt) => {
    this.setState({
      days: evt.target.value,
    });
  };
  handleNights = (evt) => {
    this.setState({
      night: evt.target.value,
    });
  };
  _handleTextFieldChange = (evt, field) => {
    // check it out: we get the evt.target.name (which will be either "email" or "password")
    // and use it to target the key on our `state` object with the same name, using bracket syntax
    this.setState({ [field]: evt.target.value });
  };
  onSubmit = () => {
    this.props.addHolidays(
      title,
      subtitle,
      short_desc,
      long_desc,
      amount,
      days,
      night,
      city_id,
      category
    );
  };
  render() {
    const { cityList } = this.props;
    const { city, days, night, amount } = this.state;
    return (
      <PageBase title='Add Holidays' navigation='Application / Add Holidays'>
        <form>
          <Grid container spacing={3}>
            <Grid item xs={6}>
              <TextField
                hintText='Title'
                label='Title'
                fullWidth={true}
                margin='normal'
                variant='outlined'
                name='title'
                onChange={this._handleTextFieldChange}
                inputProps={{ style: { fontSize: 16 } }} // font size of input text
                InputLabelProps={{ style: { fontSize: 16 } }} // font size of input label
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                hintText='Sub Title'
                label='Sub Title'
                fullWidth={true}
                margin='normal'
                variant='outlined'
                name='subtitle'
                inputProps={{ style: { fontSize: 16 } }} // font size of input text
                InputLabelProps={{ style: { fontSize: 16 } }} // font size of input label
              />
            </Grid>

            <Grid item xs={6}>
              <TextField
                id='outlined-multiline-flexible'
                label='Short Description'
                multiline
                rows={2}
                rowsMax={2}
                fullWidth={true}
                variant='outlined'
                name='short_desc'
                inputProps={{ style: { fontSize: 16 } }} // font size of input text
                InputLabelProps={{ style: { fontSize: 16 } }} // font size of input label
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                id='outlined-multiline-flexible'
                label='Long Description'
                multiline
                rows={2}
                rowsMax={4}
                fullWidth={true}
                variant='outlined'
                name='long_desc'
                inputProps={{ style: { fontSize: 16 } }} // font size of input text
                InputLabelProps={{ style: { fontSize: 16 } }} // font size of input label
              />
            </Grid>
            <Grid item xs={2}>
              <InputLabel htmlFor='City'>City</InputLabel>
              <Select
                labelId='demo-simple-select-outlined-label'
                value={city}
                label='City'
                fullWidth={true}
                onChange={this.handleCity}
                displayEmpty
                input={<BootstrapInput />}
              >
                <MenuItem value=''>
                  <em>Select City</em>
                </MenuItem>
                {cityList.cityList &&
                  cityList.cityList.length > 0 &&
                  cityList.cityList.map((cities) => (
                    <MenuItem value={cities.id}>{cities.name}</MenuItem>
                  ))}
              </Select>
            </Grid>
            <Grid item xs={2}>
              <InputLabel htmlFor='Category'>Category</InputLabel>
              <Select
                fullWidth={true}
                label='Category'
                displayEmpty
                input={<BootstrapInput />}
                onChange={this.handleCategory}
              >
                <MenuItem value={1}>Delux</MenuItem>
                <MenuItem value={2}>Super Delux</MenuItem>
                <MenuItem value={3}>Luxury</MenuItem>
              </Select>
            </Grid>
            <Grid item xs={2}>
              <InputLabel htmlFor='standard-adornment-amount'>
                Amount
              </InputLabel>
              <Input
                id='standard-adornment-amount'
                fullWidth={true}
                variant='outlined'
                onChange={this._handleTextFieldChange}
                name='amount'
                value={amount}
                inputProps={{ style: { fontSize: 16, height: 36 } }} // font size of input text
                InputLabelProps={{ style: { fontSize: 16 } }} // font size of input label
                startAdornment={
                  <InputAdornment
                    position='start'
                    inputProps={{ style: { fontSize: 16, height: 36 } }}
                  >
                    ₹
                  </InputAdornment>
                }
              />
            </Grid>
            <Grid item xs={3}>
              <InputLabel htmlFor='Days'>Days</InputLabel>
              <Select
                displayEmpty
                fullWidth={true}
                input={<BootstrapInput />}
                label='Days'
                value={days}
                onChange={this.handleDays}
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4</MenuItem>
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={6}>6</MenuItem>
                <MenuItem value={7}>7</MenuItem>
                <MenuItem value={8}>8</MenuItem>
                <MenuItem value={9}>9</MenuItem>
                <MenuItem value={10}>10</MenuItem>
              </Select>
            </Grid>
            <Grid item xs={3}>
              <InputLabel htmlFor='Nights'>Nights</InputLabel>
              <Select
                displayEmpty
                fullWidth={true}
                input={<BootstrapInput />}
                label='Nights'
                value={night}
                onChange={this.handleNights}
              >
                <MenuItem value={1}>1</MenuItem>
                <MenuItem value={2}>2</MenuItem>
                <MenuItem value={3}>3</MenuItem>
                <MenuItem value={4}>4</MenuItem>
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={6}>6</MenuItem>
                <MenuItem value={7}>7</MenuItem>
                <MenuItem value={8}>8</MenuItem>
                <MenuItem value={9}>9</MenuItem>
                <MenuItem value={10}>10</MenuItem>
              </Select>
            </Grid>
          </Grid>

          <Button
            style={styles.saveButton}
            variant='contained'
            type='submit'
            color='primary'
            onClick={this.onSubmit}
          >
            Save
          </Button>
        </form>
      </PageBase>
    );
  }
}

function mapStateToProps(state) {
  return {
    cityList: state.cities,
  };
}

const mapDispatchToProps = {
  getCities: userActions.getCities,
  addHolidays: userActions.addHolidays,
};

AddHolidays.propTypes = {
  children: PropTypes.element,
  classes: PropTypes.object,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(AddHolidays));
