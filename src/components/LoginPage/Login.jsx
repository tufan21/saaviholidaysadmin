import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { ThemeProvider } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import { grey } from "@material-ui/core/colors";
import Help from "@material-ui/icons/Help";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import theme from "../../theme";

import PropTypes from "prop-types";

import { userActions } from '../../_actions';
const styles = {
    loginContainer: {
      minWidth: 420,
      maxWidth: 500,
      height: "auto",
      position: "absolute",
      top: "20%",
      left: 0,
      right: 0,
      margin: "auto"
    },
    paper: {
      padding: 20,
      overflow: "auto"
    },
    buttonsDiv: {
      textAlign: "center",
      padding: 10
    },
    flatButton: {
      color: grey[500],
      margin: 5
    },
    checkRemember: {
      style: {
        float: "left",
        maxWidth: 180,
        paddingTop: 5
      },
      labelStyle: {
        color: grey[500]
      },
      iconStyle: {
        color: grey[500],
        borderColor: grey[500],
        fill: grey[500]
      }
    },
    loginBtn: {
      float: "right",
      fontSize: 16,
    },
    btn: {
      background: "#4f81e9",
      color: "white",
      padding: 7,
      borderRadius: 2,
      margin: 2,
      fontSize: 13
    },
    btnFacebook: {
      background: "#4f81e9"
    },
    btnGoogle: {
      background: "#e14441"
    },
    btnSpan: {
      marginLeft: 5
    },
    textField: {
      width: '90%',
      marginLeft: 'auto',
      marginRight: 'auto',            
      paddingBottom: 0,
      marginTop: 0,
      fontWeight: 500,
      fontSize:50
  },
  };
class Login extends React.Component {
   
    constructor(props) {
        super(props);

        // reset login status
        this.props.logout();

        this.state = {
            username: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        if (username && password) {
            this.props.login(username, password);
        }
    }

    render() {
        const { loggingIn } = this.props;
        const { username, password, submitted } = this.state;
        return (
            <ThemeProvider theme={theme}>
            <div>
              <div style={styles.loginContainer}>
                <Paper style={styles.paper}>
                  <form>
                    <TextField hinttext="E-mail" label="E-mail" fullWidth={true} value={username} name="username" onChange={this.handleChange}
                      inputProps={{style: {fontSize: 16}}} // font size of input text
                      InputLabelProps={{style: {fontSize: 16}}} // font size of input label
                      required
                      variant="outlined"
                    />
                    <div style={{ marginTop: 16 }}>
                      <TextField hinttext="Password" label="Password" fullWidth={true} type="password"  name="password" value={password} onChange={this.handleChange} 
                        inputProps={{style: {fontSize: 16}}} // font size of input text
                        InputLabelProps={{style: {fontSize: 16}}} // font size of input label
                        required
                        variant="outlined"
                      />
                    </div>
      
                    <div style={{ marginTop: 10 }}>
                        <Button variant="contained" color="primary" style={styles.loginBtn} onClick={this.handleSubmit.bind(this)}>
                            Login
                        </Button>
                    </div>
                  </form>
                </Paper>
              </div>
            </div>
          </ThemeProvider>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return { loggingIn };
}

const mapDispatchToProps = {
    login: userActions.login,
    logout: userActions.logout
};

Login.propTypes = {
  children: PropTypes.element,
  classes: PropTypes.object
};
export default connect(mapStateToProps, mapDispatchToProps)(Login);