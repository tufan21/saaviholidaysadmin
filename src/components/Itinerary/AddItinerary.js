import React, { useState, useEffect } from 'react';
import { grey } from '@material-ui/core/colors';
import PageBase from '../PageBase';
import RowItenary from './RowItenary';
import { withStyles } from '@material-ui/core/styles';
class AddItinerary extends React.Component {
  // const styles = {
  //   toggleDiv: {
  //     marginTop: 20,
  //     marginBottom: 5,
  //   },
  //   toggleLabel: {
  //     color: grey[400],
  //     fontWeight: 100,
  //   },
  //   buttons: {
  //     marginTop: 30,
  //     float: 'right',
  //   },
  //   saveButton: {
  //     marginLeft: 5,
  //     marginTop: 30,
  //     float: 'right',
  //     width: 100,
  //     fontSize: 18,
  //   },
  // };
  componentDidMount() {}
  render() {
    return (
      <PageBase title='Add Itenary' navigation='Application / Form Page'>
        <RowItenary />
      </PageBase>
    );
  }
}

export default withStyles(null, { withTheme: true })(AddItinerary);
