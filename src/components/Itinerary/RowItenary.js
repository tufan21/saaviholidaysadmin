import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { grey } from '@material-ui/core/colors';
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import PageBase from '../PageBase';
import Grid from '@material-ui/core/Grid';
import { Input, InputAdornment } from '@material-ui/core';

const RowItenary = () => {
  const styles = {
    toggleDiv: {
      marginTop: 20,
      marginBottom: 5,
    },
    toggleLabel: {
      color: grey[400],
      fontWeight: 100,
    },
    buttons: {
      marginTop: 30,
      float: 'right',
    },
    saveButton: {
      marginLeft: 5,
      marginTop: 16,
      float: 'right',
      width: 100,
      fontSize: 18,
    },
    description_text: {
      marginTop: 16,
    },
  };

  return (
    <PageBase title='Add Holidays' navigation='Application / Form Page'>
      <form>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <TextField
              hintText='Title'
              label='Title'
              fullWidth={true}
              margin='normal'
              variant='outlined'
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              id='outlined-multiline-flexible'
              label='Long Description'
              multiline
              rowsMax={2}
              fullWidth={true}
              style={styles.description_text}
              fullWidth={true}
              variant='outlined'
            />
          </Grid>
        </Grid>

        <Button
          style={styles.saveButton}
          variant='contained'
          type='submit'
          color='primary'
        >
          Save
        </Button>
      </form>
    </PageBase>
  );
};

export default RowItenary;
