import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../../_helpers';
import { alertActions } from '../../_actions';
import { PrivateRoute } from '../../_components';
import { HomePage } from '../HomePage/HomePage';
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Header from "./../../components/Header";
import LeftDrawer from "./../../components/LeftDrawer";
import RightDrawer from "./../../components/RightDrawer";
import AddHolidays from "./../../components/Holiday/AddHolidays";
import HolidayList from "./../../components/Holiday/HolidayList";
import Data from "../../data";
import Cities from "./../../components/Table/Cities";
import AddItinerary from "./../../components/Itinerary/AddItinerary";
// import NotFound from "./NotFoundPage";
import { ThemeProvider } from "@material-ui/core/styles";
import defaultTheme, { customTheme } from "../../theme";

const styles = () => ({
  container: {
    margin: "80px 20px 20px 15px",
    paddingLeft: defaultTheme.drawer.width,
    [defaultTheme.breakpoints.down("sm")]: {
      paddingLeft: 0
    }
    // width: `calc(100% - ${defaultTheme.drawer.width}px)`
  },
  containerFull: {
    paddingLeft: defaultTheme.drawer.miniWidth,
    [defaultTheme.breakpoints.down("sm")]: {
      paddingLeft: 0
    }
  },
  settingBtn: {
    top: 80,
    backgroundColor: "rgba(0, 0, 0, 0.3)",
    color: "white",
    width: 48,
    right: 0,
    height: 48,
    opacity: 0.9,
    padding: 0,
    zIndex: 999,
    position: "fixed",
    minWidth: 48,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0
  }
});


class App extends React.Component {
    constructor(props) {
        super(props);
        history.listen((location, action) => {
            // clear alert on location change
            this.props.clearAlerts();
        });
        // nav bar default open in desktop screen, and default closed in mobile screen
        this.state = {
          theme: defaultTheme,
          rightDrawerOpen: false,
          navDrawerOpen:
            window && window.innerWidth && window.innerWidth >= defaultTheme.breakpoints.values.md
              ? true
              : false
        };
    
        this.handleChangeRightDrawer = this.handleChangeRightDrawer.bind(this);
        this.handleChangeNavDrawer = this.handleChangeNavDrawer.bind(this);
        this.handleChangeTheme = this.handleChangeTheme.bind(this);
      }
    
      handleChangeNavDrawer() {
        this.setState({
          navDrawerOpen: !this.state.navDrawerOpen
        });
      }
    
      handleChangeRightDrawer() {
        this.setState({
          rightDrawerOpen: !this.state.rightDrawerOpen
        });
      }
    
      handleChangeTheme(colorOption) {
        const theme = customTheme({
          palette: colorOption
        });
        this.setState({
          theme
        });
      }
    render() {
        const { classes } = this.props;
        const { navDrawerOpen, rightDrawerOpen, theme } = this.state;
    
        return (
          <ThemeProvider theme={theme}>
            <Header handleChangeNavDrawer={this.handleChangeNavDrawer} navDrawerOpen={navDrawerOpen} />

            <LeftDrawer
              navDrawerOpen={navDrawerOpen}
              handleChangeNavDrawer={this.handleChangeNavDrawer}
              menus={Data.menus}
            />
              {/* <ButtonBase
                color="inherit"
                classes={{ root: classes.settingBtn }}
                onClick={this.handleChangeRightDrawer}
              >
                <i className="fa fa-cog fa-3x" />
              </ButtonBase> */}
            <RightDrawer
              rightDrawerOpen={rightDrawerOpen}
              handleChangeRightDrawer={this.handleChangeRightDrawer}
              handleChangeTheme={this.handleChangeTheme}
            />
            <div className={classNames(classes.container, !navDrawerOpen && classes.containerFull)}>
                <Switch>
                  <PrivateRoute exact path="/" component={HomePage} />
                  <Route exact path="/cities" component={Cities} />
                  <Route exact path="/holidays/add" component={AddHolidays} />
                  <Route exact path="/holidays/list" component={HolidayList} />
                  <Route exact path="/holidays/itinerary" component={AddItinerary} />
              </Switch>
            </div>
      </ThemeProvider>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return { alert };
}

const mapDispatchToProps = {
    clearAlerts: alertActions.clear
};

App.propTypes = {
  children: PropTypes.element,
  classes: PropTypes.object
};
export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));