
import React from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { App } from './components/App';
import Login from "./components/LoginPage/Login"
class Main extends React.Component {
    render() {
        return (
            <div>
                <Switch>
                  <Router exact path='/login' component={Login} />
                  <Router path='/' component={App} />
              </Switch>
            </div>
        );
    }
}


export default Main;
