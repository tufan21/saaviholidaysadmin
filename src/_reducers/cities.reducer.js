import { userConstants } from '../_constants';

export function cities(state = {}, action) {
  switch (action.type) {
    case userConstants.GET_ALL_CITIES_REQUEST:
      return {
        loading: true,
        cityList: [],
      };
    case userConstants.GET_ALL_CITIES_SUCCESS:
      return {
        cityList: action.cities,
      };
    case userConstants.GET_ALL_CITIES_FAILURE:
      return {
        error: action.error,
        cityList: [],
      };
    default:
      return state;
  }
}
